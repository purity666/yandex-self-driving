#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
#include <algorithm>
#include <vector>
#include <map>

struct Point
{
	double x, y, z;
};

struct Line
{
	double a, b;
};

struct Plane
{
	double a, b, c, d;
};

using namespace std;

void filtrate(vector<Point>& points, double p);
inline double det(double A[3][3]);
void CramerSolver(double A[3][3], double b[3], double X[3]);
void find_earth_plane(const vector<Point>& lp, Plane &earth_plane);

void dump(vector<Point> lp, const char *filename);

template<typename KeyType, typename ValueType> 
std::pair<KeyType,ValueType> get_max( const std::map<KeyType,ValueType>& x ) 
{
  using pairtype=std::pair<KeyType,ValueType>; 
  return *std::max_element(x.begin(), x.end(), [] (const pairtype & p1, const pairtype & p2) 
  {
        return p1.second < p2.second;
  }); 
}



int main()
{
	cout << setprecision(6) << fixed;
	ifstream data("input.txt");
	
	double p;
	int count_p;
	
	data >> p >>
			count_p;
			
	vector<Point> lp;
	
	for (int i = 0; i < count_p; i++)
	{
		Point p;
		data >> p.x >> 
				p.y >> 
				p.z;
		
		lp.push_back(p);
	}
	data.close();
	
	filtrate(lp, p);
	//~ dump(lp, "filter");
	
	Point x_z, y_z, z_z;
	
	sort(lp.begin(), lp.end(), 
		[](const Point &a, const Point &b)
		{
			return a.z < b.z;
		});
	
	Plane earth_plane;
	find_earth_plane(lp, earth_plane);
	
	cout << earth_plane.a << " " << earth_plane.b << " " << earth_plane.c << " " << earth_plane.d << endl;
	//~ cout << earth_plane.a/earth_plane.c << "*x + " << earth_plane.b/earth_plane.c << "*y + " << earth_plane.d/earth_plane.c << endl;
	
	return 0;
}

/// ////////////////////////////////////////////////////////////////////
void filtrate(vector<Point>& points, double p)
{
	map<double,int> dup;
	vector<double> top_z;
	for_each( points.begin(), points.end(), [&dup]( const Point& val ){ dup[val.z]++; } );
	
	double percent = 0.0;
	int p_size = points.size();
	
	for (; percent < 0.5; )
	{
		auto max_item = get_max(dup);
		
		percent += max_item.second / (double)p_size;
		top_z.push_back(max_item.first);
		
		dup[max_item.first] = 0;
	}
	
	if (top_z.size() < 3)
	{
		auto max_item = get_max(dup);
		
		percent += max_item.second / (double)p_size;
		top_z.push_back(max_item.first);
		
		dup[max_item.first] = 0;
	}
	
	sort(top_z.begin(), top_z.end());
	
	vector<Point> POINTS;
	
	/// now must include all points in range [-p : p]
	for (auto i = points.begin(); i != points.end(); i++)
	{
		auto z = lower_bound(top_z.begin(), top_z.end(), i->z);
		
		if (z == top_z.end() || fabs(*z - i->z) > p)
		{
			continue;
		}
		
		POINTS.push_back(*i);
	}
	
	points.clear();
	points = POINTS;
}

inline double det(double A[3][3])
{
    return A[0][0] * A[1][1] * A[2][2] +
           A[0][1] * A[1][2] * A[2][0] +
           A[1][0] * A[2][1] * A[0][2] -
           A[2][0] * A[1][1] * A[0][2] -
           A[1][0] * A[0][1] * A[2][2] -
           A[2][1] * A[1][2] * A[0][0];
}

void CramerSolver(double A[3][3], double b[3], double X[3])
{
	double detA;
    int i, j;
	double T[3][3];
 
    detA = det(A);
 
    for (int n = 0; n < 3; n++)
    {
        for (i = 0; i < 3; i++)
            for (j = 0; j < 3; j++)
                T[i][j] = A[i][j];
 
        for (j = 0; j < 3; j++)
            T[j][n] = b[j];
 
        X[n] = det(T) / detA;
    }
}

void find_earth_plane(const vector<Point>& lp, Plane &earth_plane)
{
	/// Let be Ax + By + D = -z
	double A[3][3] = { 0 };
	double b[3] = { 0 };
	double Solution[3];
	int n = lp.size();
	for (int i = 0; i < n; i++)
	{
		A[0][0] += lp[i].x*lp[i].x;
		A[0][1] += lp[i].x*lp[i].y;
		A[0][2] += lp[i].x;
		A[1][0] += lp[i].x*lp[i].y;
		A[1][1] += lp[i].y*lp[i].y;
		A[1][2] += lp[i].y;
		A[2][0] = A[0][2];
		A[2][1] = A[1][2];
		
		b[0] += lp[i].x*lp[i].z;
		b[1] += lp[i].y*lp[i].z;
		b[2] += lp[i].z;
	}
	A[2][2] = n;

	CramerSolver(A, b, Solution);
	
	earth_plane.a = -Solution[0];
	earth_plane.b = -Solution[1];
	earth_plane.c = 1;
	earth_plane.d = -Solution[2];
}

void dump(vector<Point> lp, const char *filename)
{
	ofstream dump(filename);			
	for (auto point: lp)
	{
		dump << point.x << " " << point.y << " " << point.z << endl;
	}
	dump.close();
}
